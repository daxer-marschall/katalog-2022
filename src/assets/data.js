export const DATA = [
    {
        id: 1,
        title: 'Alte Bäume, um 1845-9',
        artist: 'Peder Balke (Hedemarken, Norwegen 1804 - 1887 Oslo)',
        image: 'images/K756_Balke-1.jpg'
    },
    {
        id: 2,
        title: 'Landschaft von  Finnmark, um 1860',
        artist: 'Peder Balke (Hedemarken, Norwegen 1804 - 1887 Oslo)',
        image: 'images/K757_Balke.jpg'
    },
    {
        id: 3,
        title: 'Arkadische Landschaft, 1814',
        artist: 'Jean-Victor Bertin (Paris 1767 - 1842 Paris)',
        image: 'images/Bertin_Landscape_Tefaf-scaled.jpg'
    },
    {
        id: 4,
        title: 'Panorama des Forum Romanum, 1859',
        artist: 'Johann Jakob Frey (Basel 1813 - 1865 Frascati)',
        image: 'images/K760_Frey_web-Kopie.jpg'
    },
    {
        id: 5,
        title: 'IMG_3109.jpeg',
        artist: '',
        image: 'Bilder/IMG_3109.jpeg'
    },    
    {
        id: 6,
        title: 'IMG_3110.jpeg',
        artist: '',
        image: 'Bilder/IMG_3110.jpeg'
    },    
    {
        id: 7,
        title: 'IMG_3111.jpeg',
        artist: '',
        image: 'Bilder/IMG_3111.jpeg'
    },
    {
        id: 8,
        title: 'IMG_3112.jpeg',
        artist: '',
        image: 'Bilder/IMG_3112.jpeg'
    },    
    {
        id: 9,
        title: 'IMG_3113.jpeg',
        artist: '',
        image: 'Bilder/IMG_3113.jpeg'
    },    
    {
        id: 10,
        title: 'IMG_3114.jpeg',
        artist: '',
        image: 'Bilder/IMG_3114.jpeg'
    },    
    {
        id: 11,
        title: 'IMG_3115.jpeg',
        artist: '',
        image: 'Bilder/IMG_3115.jpeg'
    },    
    {
        id: 12,
        title: 'IMG_3116.jpeg',
        artist: '',
        image: 'Bilder/IMG_3116.jpeg'
    },    
    {
        id: 13,
        title: 'IMG_3117.jpeg',
        artist: '',
        image: 'Bilder/IMG_3117.jpeg'
    },    
    {
        id: 14,
        title: 'IMG_3118.jpeg',
        artist: '',
        image: 'Bilder/IMG_3118.jpeg'
    },    
    {
        id: 15,
        title: 'IMG_3119.jpeg',
        artist: '',
        image: 'Bilder/IMG_3119.jpeg'
    },    
    {
        id: 16,
        title: 'IMG_3120.jpeg',
        artist: '',
        image: 'Bilder/IMG_3120.jpeg'
    },    
    {
        id: 17,
        title: 'IMG_3121.jpeg',
        artist: '',
        image: 'Bilder/IMG_3121.jpeg'
    },    
    {
        id: 18,
        title: 'IMG_3122.jpeg',
        artist: '',
        image: 'Bilder/IMG_3122.jpeg'
    },
    {
        id: 19,
        title: 'IMG_3123.jpeg',
        artist: '',
        image: 'Bilder/IMG_3123.jpeg'
    },
]